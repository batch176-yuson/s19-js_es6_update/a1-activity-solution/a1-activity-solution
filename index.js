let num = 7;
let getCube = num ** 3;

console.log(`the cube of ${num} is ${getCube}`);

let address = ["258 Washington Ave", "NW, Califonia", 90011];
const[street,state,zip] = address;

console.log(`I live at ${street} ${state} ${zip}`);

let animal = {
	name: "Lolong",
	type: "saltwater crocodile",
	weight: 1075,
	height: "20ft 3in"
};

const {name, type, weight, height} = animal;

console.log(`${name} was a ${type}. He weighed at ${weight}kgs with a measurement of ${height}.`);

let numbers = [1,2,3,4,5];
			
numbers.forEach((number) => console.log(number))

class Dog {
	constructor(name, age, breed){
		this.name = name
		this.age = age
		this.breed = breed
	}

}

const myDog = new Dog(`Bruce`, 1, `American Bully`)
console.log(myDog);
